<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DateTime;

class CalendrierController extends Controller
{
    public function index()
    {
    	$ddate = date('Y-m-d');
		$date = new DateTime($ddate);
		$week = $date->format("W");
        return view('Calendrier')->withWeek($week);
    }
}
