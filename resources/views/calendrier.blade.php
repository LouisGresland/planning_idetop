@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
           Page du calendrier

           <table>

           	
   			<caption>Semaine <?php echo $week; ?></caption>
           <tbody>
        <tr>
           <td>Jours</td>
           <td>Equipe</td>
           <td>N°Affaire</td>
           <td>Client</td>
           <td>Temps Production</td>
           <td>Action réalisée</td>
           <td>Statut</td>
       </tr>

       <?php
       for ($jours_semaine = 1; $jours_semaine <= 5; $jours_semaine++) { ?>
       	<tr>
       		<?php if ($jours_semaine == 1){
			    ?>
           <td rowspan=25>Lundi </td>
           <?php
           }

           if ($jours_semaine == 2){
			    ?>
           <td rowspan=25>Mardi </td>
           <?php
           }

           if ($jours_semaine == 3){
			    ?>
           <td rowspan=25>Mercredi </td>
           <?php
           }

           if ($jours_semaine == 4){
			    ?>
           <td rowspan=25>Jeudi </td>
           <?php
           }

           if ($jours_semaine == 5){
			    ?>
           <td rowspan=25>Vendredi </td>
           <?php
           }

			for ($nombre_de_lignes = 1; $nombre_de_lignes <= 25; $nombre_de_lignes++)

			{
			if ($nombre_de_lignes == 1){
			    ?>
           <td rowspan=5> <a href="">Valérie</a></td>
           <?php
           }

           if ($nombre_de_lignes == 6){
			    ?>
           <td rowspan=5> <a href="">Louis</a></td>
           <?php
           }

           if ($nombre_de_lignes == 11){
			    ?>
           <td rowspan=5><a href="">François</a></td>
           <?php
           }

           if ($nombre_de_lignes == 16){
			    ?>
           <td rowspan=5><a href="">Pauline</a></td>
           <?php
           }

           if ($nombre_de_lignes == 21){
			
			?>
           <td rowspan=5><a href="">Candice</a></td>
           <?php
           }

           ?>

           <td><input type=text></input></td>
           <td><input type=text></input></td>
           
           <td><select>

           <option value="" selected></option>

           <?php
			$start = "00:05";
			$end = "6:00";

			$tStart = strtotime($start);
			$tEnd = strtotime($end);
			$tNow = $tStart;



			while($tNow <= $tEnd){
				$heure = date("H:i",$tNow); 
				echo '<option>'.$heure;
			  $tNow = strtotime('+5 minutes',$tNow);
			  echo '</option>';	}
			?>
			</select>
			</td>
           <td><input type=text></input></td>
           <td>
	            <SELECT>
					<OPTION value="" selected></option>
					<OPTION value="1">En cours </option>
					<OPTION value="2">Fini </option>
				</SELECT>
			</td>

       </tr>
       <?php
			}
		}
			?>
           </tbody>
		</table>

           
        </div>
    </div>
</div>
@endsection