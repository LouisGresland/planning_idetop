@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Administration d'IDETOP</div>

                <div class="panel-body">
                    <a href="{{ url('/calendrier') }}">Calendrier</a> <br/>
                    <a href="{{ url('/projet') }}">Projet</a> <br/>
                    <a href="{{ url('/email') }}">Email</a> <br/>
                    <a href="{{ url('/memocode') }}">Memocode</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
