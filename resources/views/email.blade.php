@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
           Page d'email
           <form>
            <div class="form-group">
              <label name="email">Email :</label>
              <input id="email" name="email" class="form-control">
            </div>

            <div class="form-group">
              <label name="subject">Sujet :</label>
              <input id="subject" name="subject" class="form-control">
            </div>

            <div class="form-group">
              <label name="message">Message:</label>
              <textarea id="message" name="message" class="form-control">Ecriver votre message...</textarea>
            </div>

            <input type="submit" value="Envoyer" class="btn btn-success">
          </form>

           
        </div>
    </div>
</div>
@endsection